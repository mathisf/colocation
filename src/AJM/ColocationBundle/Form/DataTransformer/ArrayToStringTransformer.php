<?php
namespace AJM\ColocationBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ArrayToStringTransformer implements DataTransformerInterface
{

    public function transform($array)
    {
        return $array[0];
    }


    public function reverseTransform($string)
    {
        return array($string);
    }
}