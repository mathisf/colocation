<?php

namespace AJM\ColocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AJM\ColocationBundle\Form\Logement;

class ColocationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut',      DateType::class, array('label'=>'colocation.datedeb'))
            ->add('dateFin',      DateType::class, array('label'=>'colocation.datefin','required' => false))
            ->add('titreAnnonce',     TextType::class, array('label'=>"colocation.titre"))
            ->add('nbPlaces',   IntegerType::class, array('label'=>"colocation.nbplaces"))
            ->add('loyer',   IntegerType::class, array('label'=>"colocation.loyer"))
            ->add('logement',     LogementType::class, array('label'=>"colocation.logement.logement"))
            ->add('descAnnonce',   TextareaType::class, array('label'=>'colocation.description','required' => false))
            ->add('ajouter',      SubmitType::class, array('label'=>'bouton.ajouter.colocation'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AJM\ColocationBundle\Entity\Colocation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ajm_colocationbundle_colocation';
    }


}
