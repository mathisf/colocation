<?php

namespace AJM\ColocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AJM\ColocationBundle\Form\Adresse;

class LogementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type',       ChoiceType::class, array(
                'choices'  => array(
                    'colocation.logement.appartement' => 'Appartement',
                    'colocation.logement.maison' => 'Maison',
                ), 'label' => 'colocation.logementtype'))
            ->add('adresse',     AdresseType::class, array('label' => 'colocation.logement.adresse'))
            ->add('surface',      IntegerType::class, array('label' => 'colocation.logement.surface') )
            ->add('nbPieces',     IntegerType::class, array('label' => 'colocation.logement.nbpieces'))
            ->add('nbChambres',   IntegerType::class, array('label' => 'colocation.logement.nbchambres'))
            ->add('nbSallesDeau',   IntegerType::class, array('label' => "colocation.logement.nbsallesdo"));

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AJM\ColocationBundle\Entity\Logement'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ajm_colocationbundle_logement';
    }


}
