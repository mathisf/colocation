<?php

namespace AJM\ColocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ColocationEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('ajouter',      SubmitType::class)
            ->add('Modifier', SubmitType::class, array('label'=>'bouton.modifier.colocation'));
    }
    
    public function getParent(){
        return ColocationType::class;
    }

}
