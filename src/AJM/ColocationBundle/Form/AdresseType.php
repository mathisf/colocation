<?php

namespace AJM\ColocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AdresseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numRue',      IntegerType::class, array('label' => 'colocation.adresse.numrue','required' => false))
            ->add('nomRue',     TextType::class, array('label' => 'colocation.adresse.nomrue'))
            ->add('etage',   IntegerType::class, array('label' => 'colocation.adresse.etage','required' => false))
            ->add('ville',     TextType::class, array('label' => 'colocation.adresse.ville'))
            ->add('cp',   IntegerType::class, array('label' => 'colocation.adresse.cp'))
            ->add('pays',     TextType::class, array('label' => 'colocation.adresse.pays'));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AJM\ColocationBundle\Entity\Adresse'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ajm_colocationbundle_adresse';
    }


}
