<?php

namespace AJM\ColocationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AJM\ColocationBundle\Entity\Colocation;
use AJM\ColocationBundle\Form\ColocationType;
use AJM\ColocationBundle\Form\ColocationEditType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ColocationController extends Controller
{

    /**
     * @Route("/{_locale}/", name="ajm_colocation_homepage")
    */
    public function indexAction()
    {

        $repository = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AJMColocationBundle:Colocation');

        $listAnnonces = $repository->findAll();
        return $this->render('@AJMColocation/Colocation/index.html.twig', array(
            'listAnnonces' => $listAnnonces
          ));
    }

    /**
     * @Route("/{_locale}/mesAnnonces", name="ajm_colocation_mesAnnonces")
     * @Security("has_role('ROLE_USER')")
    */
    public function mesAnnoncesAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.annonces'), $this->get("router")->generate("ajm_colocation_mesAnnonces"));

        $user = $this->getUser();
        $repository = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AJMColocationBundle:Colocation');
                             
        $listMesAnnonces = $repository->findByUser($user);

        return $this->render('@AJMColocation/Colocation/mesAnnonces.html.twig', array(
            'listMesAnnonces' => $listMesAnnonces
          ));
    }

    /**
     * @Route("/{_locale}/ajoutAnnonce", name="ajm_colocation_ajoutAnnonce")
     * @Security("has_role('ROLE_USER')")
    */
    public function ajoutAnnonceAction(Request $request)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.annonces'), $this->get("router")->generate("ajm_colocation_mesAnnonces"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.ajouterAnn'), $this->get("router")->generate("ajm_colocation_ajoutAnnonce"));

        $colocation = new Colocation();
        $form   = $this->get('form.factory')->create(ColocationType::class, $colocation);
    
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $colocation->setUser($user);
            $em->persist($colocation);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirectToRoute('ajm_colocation_mesAnnonces');
        }
    
        return $this->render('@AJMColocation/Colocation/ajoutColocation.html.twig', array(
          'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{_locale}/modifierAnnonce/{id}", requirements={"id": "\d+"}, name="ajm_colocation_modifierAnnonce")
     * @Security("has_role('ROLE_USER')")
    */
    public function modifierAnnonceAction($id, Request $request)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.annonces'), $this->get("router")->generate("ajm_colocation_mesAnnonces"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.modAnn'), $this->get("router"));

        $em = $this->getDoctrine()->getManager();
        $colocation = $em->getRepository('AJMColocationBundle:Colocation')->find($id);
        if ($colocation != null) {
            $user1 = $colocation->getUser();
            $user2 = $this->getUser();
    
            if (!($user1->getUsername() == $user2->getUsername()) && !$user2->hasRole('ROLE_ADMIN')) {
                return $this->redirectToRoute('ajm_colocation_homepage');
            } 
        }
        $form   = $this->get('form.factory')->create(ColocationEditType::class, $colocation);
    
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->persist($colocation);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirectToRoute('ajm_colocation_mesAnnonces');
        }
    
        return $this->render('@AJMColocation/Colocation/modificationColocation.html.twig', array(
          'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{_locale}/supprimerAnnonce/{id}", requirements={"id": "\d+"}, name="ajm_colocation_supprimerAnnonce")
     * @Security("has_role('ROLE_USER')")
    */
    public function supprimerAnnonceAction($id)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.annonces'), $this->get("router")->generate("ajm_colocation_mesAnnonces"));
        $breadcrumbs->addItem(" Supprimer une annonce ", $this->get("router"));

        $em = $this->getDoctrine()->getManager();
        $colocation = $em->getRepository('AJMColocationBundle:Colocation')->find($id);

        if ($colocation != null) {
            $user1 = $colocation->getUser();
            $user2 = $this->getUser();
    
            if (!($user1->getUsername() == $user2->getUsername()) && !$user2->hasRole('ROLE_ADMIN')) {
                return $this->redirectToRoute('ajm_colocation_homepage');
            } 
        }

        if($colocation == null) {
            return $this->redirectToRoute('ajm_colocation_mesAnnonces', []);
        } else {
            $em->remove($colocation);
            $em->flush();
            $user = $this->getUser();
            $repository = $this->getDoctrine()
                                ->getManager()
                                ->getRepository('AJMColocationBundle:Colocation');
    
            $listMesAnnonces = $repository->findByUser($user);
    
            return $this->render('@AJMColocation/Colocation/mesAnnonces.html.twig', array(
                'listMesAnnonces' => $listMesAnnonces
            ));
        }
    }

    /**
     * @Route("/{_locale}/detailsAnnonce/{id}", requirements={"id": "\d+"}, name="ajm_colocation_detailsAnnonce")
    */
    public function detailsAnnonceAction($id)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.annDet'), $this->get("router"));

        $em = $this->getDoctrine()->getManager();
        $colocation = $em->getRepository('AJMColocationBundle:Colocation')->find($id);
    
            return $this->render('@AJMColocation/Colocation/detailsColocation.html.twig', array(
                'annonce' => $colocation
            ));

    }

   /**
     * @Route("/{_locale}/recherche", requirements={"terme": "\d+", "_method": "GET"}, name="ajm_colocation_recherche")
    */
    public function rechercheAction(Request $request)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.recherche'), $this->get("router"));

        $entree = $request->query->get('q', '');
        $queries = explode(" ", $entree);
        
        $toutesLesColocations = [];
        $idColocRecherche=[];
        $resultatRecherche = [];
        for ($i=0; $i < count($queries); $i++) { 
            $query = $queries[$i];        
            
            $colocations = $this->getDoctrine()->getRepository(Colocation::class)->findColocationByKeyword($query);
            
            foreach ($colocations as $colocation) {
                $toutesLesColocations[] = $colocation['id'];
            }
                
        }

        /* Si il y a plus de 1 mot dans la recherche, un traitement specifique est realise
         * Cet algo ne va garder que les doublons
         */
        if(count($queries) > 1){
            $array_unique = array_unique($toutesLesColocations); 

            if (count($toutesLesColocations) - count($array_unique)){ 
                for ($i=0; $i<count($toutesLesColocations); $i++) {
                    if (!array_key_exists($i, $array_unique)) 
                        $idColocRecherche[] = $toutesLesColocations[$i];
                } 
            }
        }else{
            $idColocRecherche = $toutesLesColocations;
        }

        foreach ($idColocRecherche as $id) {
            $colocation = $this->getDoctrine()->getRepository('AJMColocationBundle:Colocation')->find($id);
            $resultatRecherche[] = $colocation;
        }

        return $this->render('@AJMColocation/Colocation/recherche.html.twig', array(
            'colocations' => $resultatRecherche
        ));
    }

   /**
     * @Route("/{_locale}/recherchePersonnalisee", requirements={"terme": "\d+", "_method": "GET"}, name="ajm_colocation_recherchePersonnalisee")
    */
    public function recherchePersonnaliseeAction(Request $request)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.rechercheAv'), $this->get("router"));

        $type = $request->query->get('type');
        $ville = $request->query->get('ville');
        $prixMin = $request->query->get('prixMin');
        $prixMax = $request->query->get('prixMax');
        $surfaceMin = $request->query->get('surfaceMin');
        $nbPiece = $request->query->get('nbPiece');

        if(empty($type) && empty($ville) && empty($prix) && empty($prixMin) && empty($prixMax) && empty($surfaceMin) && empty($nbPiece)){
            $toutesLesColocations=[];
        }else{
            $toutesLesColocations = $this->getDoctrine()->getRepository(Colocation::class)->findColocationByMultipleKeyword($type,$ville,$prixMin,$prixMax,$surfaceMin,$nbPiece);
        }
        
        
        return $this->render('@AJMColocation/Colocation/rechercheAvancee.html.twig', array(
            'colocations' => $toutesLesColocations
        ));
    }

    /**
     * @Route("/{_locale}/fluxRSS", defaults={"_format"="xml"}, name="ajm_colocation_fluxRSS")
    */
    public function fluxRSSAction()
    {
        $repository = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AJMColocationBundle:Colocation');

        $listAnnonces = $repository->findAll();

        return $this->render('@AJMColocation/Colocation/fluxRSS.xml.twig', array(
            'listAnnonces' => $listAnnonces
          ));
    }    
}
