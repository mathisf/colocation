<?php

namespace AJM\ColocationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AJM\ColocationBundle\Entity\Demandes;
use AJM\ColocationBundle\Entity\Colocation;
use AJM\ColocationBundle\Form\DemandesType;
use AJM\ColocationBundle\Form\DemandesEditType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DemandeController extends Controller
{
    
    /**
     * @Route("/{_locale}/ajoutDemande/{id}", requirements={"id": "\d+"}, name="ajm_demande_ajoutDemande")
     * @Security("has_role('ROLE_USER')")
    */
    public function ajoutDemandeAction($id, Request $request)
    {  
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.envDem'), $this->get("router")->generate("ajm_demande_mesDemandes"));

        $demande = new Demandes();
        $form   = $this->get('form.factory')->create(DemandesType::class, $demande);
       
        $em = $this->getDoctrine()->getManager();
        $colocation = $em->getRepository('AJMColocationBundle:Colocation')->find($id);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $user = $this->getUser();
            $demande->setEtat("En attente");
            $demande->setUser($user);
            $demande->setColocation($colocation);
            $em->persist($demande);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Demande bien enregistrée.');

            return $this->redirectToRoute('ajm_demande_mesDemandes');
        }
    
        return $this->render('@AJMColocation/Demandes/ajoutDemande.html.twig', array(
            'colocation' => $colocation,
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/{_locale}/mesDemandes", name="ajm_demande_mesDemandes")
     * @Security("has_role('ROLE_USER')")
    */
    public function mesDemandesAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.demandesEnvoyees'), $this->get("router")->generate("ajm_demande_mesDemandes"));

        $user = $this->getUser();
        $repository = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AJMColocationBundle:Demandes');

        $listMesDemandes = $repository->findByUser($user);    

        return $this->render('@AJMColocation/Demandes/mesDemandes.html.twig', array(
            'listMesDemandes' => $listMesDemandes
          ));
    }

    /**
     * @Route("/{_locale}/modifierDemande/{id}", requirements={"id": "\d+"}, name="ajm_demande_modifierDemande")
     * @Security("has_role('ROLE_USER')")
    */
    public function modifierDemandeAction($id, Request $request)
    {   
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.demandesEnvoyees'), $this->get("router")->generate("ajm_demande_mesDemandes"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.modDeam'), $this->get("router"));

        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('AJMColocationBundle:Demandes')->find($id);

        if ($demande != null) {
            $user1 = $demande->getUser();
            $user2 = $this->getUser();
    
            if (!($user1->getUsername() == $user2->getUsername()) && !$user2->hasRole('ROLE_ADMIN')) {
                return $this->redirectToRoute('ajm_colocation_mesADemandes');
            } 
        }

        $form   = $this->get('form.factory')->create(DemandesEditType::class, $demande);
    
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->persist($demande);
            $em->flush();

            //$request->getSession()->getFlashBag()->add('notice', 'Demande bien enregistrée.');

            return $this->redirectToRoute('ajm_demande_mesDemandes');
        }
    
        return $this->render('@AJMColocation/Demandes/modificationDemande.html.twig', array(
          'demande' => $demande,
          'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{_locale}/supprimerDemande/{id}", requirements={"id": "\d+"}, name="ajm_demande_supprimerDemande")
     * @Security("has_role('ROLE_USER')")
    */
    public function supprimerDemandeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('AJMColocationBundle:Demandes')->find($id);

        if ($demande != null) {
            $user1 = $demande->getUser();
            $user2 = $this->getUser();
    
            if (!($user1->getUsername() == $user2->getUsername()) && !$user2->hasRole('ROLE_ADMIN')) {
                return $this->redirectToRoute('ajm_colocation_mesDemandes');
            } 
        }

        if($demande == null) {
            return $this->redirectToRoute('ajm_colocation_mesDemandes', []);
        } else {
            $em->remove($demande);
            $em->flush();
            $user = $this->getUser();
            $repository = $this->getDoctrine()
                                ->getManager()
                                ->getRepository('AJMColocationBundle:Demandes');
    
            $listMesDemandes = $repository->findByUser($user);
    
            return $this->render('@AJMColocation/Demandes/mesDemandes.html.twig', array(
                'listMesDemandes' => $listMesDemandes
              ));
        }
    }

    /**
     * @Route("/{_locale}/mesDemandesRecues", name="ajm_demande_mesDemandesRecues")
     * @Security("has_role('ROLE_USER')")
    */
    public function mesDemandesRecuesAction()
    {   
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.accueil'), $this->get("router")->generate("ajm_colocation_homepage"));
        $breadcrumbs->addItem($this->get('translator')->trans('ariane.demandesRecues'), $this->get("router")->generate("ajm_demande_mesDemandes"));

        $listeDemandesrecues = $this->mesDemandesRecuesSousFonction();

        return $this->render('@AJMColocation/Demandes/mesDemandesRecues.html.twig', array(
            'listMesDemandesRecues' => $listeDemandesrecues
          ));
    }

    public function mesDemandesRecuesSousFonction(){
        $em = $this->getDoctrine()->getManager();
        $listeDemandesrecues = array(); 
        $user = $this->getUser();
        $repositoryColoc = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AJMColocationBundle:Colocation');

        $listAnnonces = $repositoryColoc->findByUser($user);

        foreach ($listAnnonces as $annonce) {
            $listeDemandesrecues[] = $annonce->getDemandes();
        }

        return $listeDemandesrecues;
    }

    /**
     * @Route("/{_locale}/accepterDemande/{id}", requirements={"id": "\d+"}, name="ajm_demande_accepterDemande")
     * @Security("has_role('ROLE_USER')")
    */
    public function accepterDemandeAction($id)
    {   
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('AJMColocationBundle:Demandes')->find($id);
        $demande->setEtat("Accepté");
        $colocation = $demande->getColocation();
        $nbPlacesDispo = $colocation->getNbPlaces();
        if($nbPlacesDispo > 0){
            $colocation->setNbPlaces($nbPlacesDispo-1);
            $em->persist($colocation);
            $retourUtilisateur = $this->get('translator')->trans('demande.accept');
        }else{
            $retourUtilisateur = $this->get('translator')->trans('demande.complet');
            $demande->setEtat("Refusé");
        }
        $em->persist($demande);
        $em->flush();
        $listeDemandesrecues = $this->mesDemandesRecuesSousFonction();

        return $this->render('@AJMColocation/Demandes/mesDemandesRecues.html.twig', array(
            'listMesDemandesRecues' => $listeDemandesrecues,
            'retourUtilisateur' => $retourUtilisateur
          ));
    }
    
    /**
     * @Route("/{_locale}/refuserDemande/{id}", requirements={"id": "\d+"}, name="ajm_demande_refuserDemande")
     * @Security("has_role('ROLE_USER')")
    */
    public function refuserDemandeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('AJMColocationBundle:Demandes')->find($id);
        $demande->setEtat("Refusé");
        $em->persist($demande);
        $em->flush();

        return $this->redirectToRoute('ajm_demande_mesDemandesRecues');

    }
    
}
