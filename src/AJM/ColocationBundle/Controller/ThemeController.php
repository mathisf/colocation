<?php
namespace AJM\ColocationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ThemeController extends Controller
{

    /**
     * @Route("/{_locale}/changeTheme", name="ajm_theme_switch")
    */
    public function switchAction()
    {
      $session = $this->get('session');
      $theme = $session->get('theme');

      if ($theme != null) {
        if($theme == '1') {
          $session->set('theme', '2');
        } else {
          $session->set('theme', '1');
        }
      } else {
        $session->set('theme', '2');
      }

      return $this->redirectToRoute('ajm_colocation_homepage', []);
    }
}
