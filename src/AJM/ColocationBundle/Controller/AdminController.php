<?php

namespace AJM\ColocationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AJM\UserBundle\Form\AdminUserType;
use AJM\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{

    /**
     * @Route("/{_locale}/admin", name="ajm_admin_indexAdmin")
     * @Security("has_role('ROLE_ADMIN')")
    */
    public function indexAdminAction()
    {
        $repository = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AJMUserBundle:User');

        $listUser = $repository->findAll();

        return $this->render('@AJMColocation/Administration/admin.html.twig', array(
            'listUser' => $listUser
          ));
    }

    /**
     * @Route("/{_locale}/admin/suppressionUtilisateur/{id}", requirements={"id": "\d+"}, name="ajm_admin_supprimerUser")
     * @Security("has_role('ROLE_ADMIN')")
    */
    public function supprimerUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AJMUserBundle:User')->find($id);

        if ($user != null) {
            $user2 = $this->getUser();
    
            if (!$user2->hasRole('ROLE_ADMIN')) {
                return $this->redirectToRoute('ajm_colocation_homepage');
            } 
        }

        if($user == null) {
            return $this->redirectToRoute('ajm_admin_indexAdmin');
        }else{
            $em->remove($user);
            $em->flush();

            return $this->redirectToRoute('ajm_admin_indexAdmin');
        }
    }

    /**
     * @Route("/{_locale}/admin/ajoutUser", name="ajm_admin_ajoutUser")
     * @Security("has_role('ROLE_ADMIN')")
    */
    public function ajoutUserAction(Request $request)
    {
        $user = new User();
        $user->setEnabled(true);
        $form   = $this->get('form.factory')->create(AdminUserType::class, $user);
        
        $user2 = $this->getUser();
    
        if (!$user2->hasRole('ROLE_ADMIN')) {
            return $this->redirectToRoute('ajm_colocation_homepage');
        } 
    
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('ajm_admin_indexAdmin');
        }
    
        return $this->render('@AJMColocation/Administration/ajoutUserAdmin.html.twig', array(
          'form' => $form->createView(),
        ));
    }
}
