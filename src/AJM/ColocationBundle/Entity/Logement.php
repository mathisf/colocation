<?php

namespace AJM\ColocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logement
 *
 * @ORM\Table(name="logement")
 * @ORM\Entity(repositoryClass="AJM\ColocationBundle\Repository\LogementRepository")
 */
class Logement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="surface", type="integer")
     */
    private $surface;

    /**
     * @var int
     *
     * @ORM\Column(name="nbPieces", type="integer")
     */
    private $nbPieces;

    /**
     * @var int
     *
     * @ORM\Column(name="nbChambres", type="integer")
     */
    private $nbChambres;

    /**
     * @var int
     *
     * @ORM\Column(name="nbSallesDeau", type="integer")
     */
    private $nbSallesDeau;

    /**
     * @ORM\OneToOne(targetEntity="AJM\ColocationBundle\Entity\Adresse", cascade={"persist", "remove"})
     */
    private $adresse;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Logement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set surface
     *
     * @param integer $surface
     *
     * @return Logement
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return int
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set nbPieces
     *
     * @param integer $nbPieces
     *
     * @return Logement
     */
    public function setNbPieces($nbPieces)
    {
        $this->nbPieces = $nbPieces;

        return $this;
    }

    /**
     * Get nbPieces
     *
     * @return int
     */
    public function getNbPieces()
    {
        return $this->nbPieces;
    }

    /**
     * Set nbChambres
     *
     * @param integer $nbChambres
     *
     * @return Logement
     */
    public function setNbChambres($nbChambres)
    {
        $this->nbChambres = $nbChambres;

        return $this;
    }

    /**
     * Get nbChambres
     *
     * @return int
     */
    public function getNbChambres()
    {
        return $this->nbChambres;
    }

    /**
     * Set nbSallesDeau
     *
     * @param integer $nbSallesDeau
     *
     * @return Logement
     */
    public function setNbSallesDeau($nbSallesDeau)
    {
        $this->nbSallesDeau = $nbSallesDeau;

        return $this;
    }

    /**
     * Get nbSallesDeau
     *
     * @return int
     */
    public function getNbSallesDeau()
    {
        return $this->nbSallesDeau;
    }

    /**
     * Set adresse
     *
     * @param \AJM\ColocationBundle\Entity\Adresse $adresse
     *
     * @return Logement
     */
    public function setAdresse(\AJM\ColocationBundle\Entity\Adresse $adresse = null)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return \AJM\ColocationBundle\Entity\Adresse
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
}
