<?php

namespace AJM\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AJMUserBundle extends Bundle
{
    public function getParent()
    {
      return 'FOSUserBundle';
    }
  
}
