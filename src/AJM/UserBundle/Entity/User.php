<?php

namespace AJM\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AJM\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser

{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=13, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\OneToMany(targetEntity="AJM\ColocationBundle\Entity\Demandes", mappedBy="user", cascade={"persist", "remove"})
     */
    private $demandes;

    /**
     * @ORM\OneToMany(targetEntity="AJM\ColocationBundle\Entity\Colocation", mappedBy="user", cascade={"persist", "remove"})
     */
    private $colocations;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Add demande
     *
     * @param \AJM\ColocationBundle\Entity\Demandes $demande
     *
     * @return User
     */
    public function addDemande(\AJM\ColocationBundle\Entity\Demandes $demande)
    {
        $this->demandes[] = $demande;
        $demande->setUser($this);

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \AJM\ColocationBundle\Entity\Demandes $demande
     */
    public function removeDemande(\AJM\ColocationBundle\Entity\Demandes $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandes()
    {
        return $this->demandes;
    }

    /**
     * Add colocation
     *
     * @param \AJM\ColocationBundle\Entity\Colocation $colocation
     *
     * @return User
     */
    public function addColocation(\AJM\ColocationBundle\Entity\Colocation $colocation)
    {
        $this->colocations[] = $colocation;
        $colocation->setUser($this);

        return $this;
    }

    /**
     * Remove colocation
     *
     * @param \AJM\ColocationBundle\Entity\Colocation $colocation
     */
    public function removeColocation(\AJM\ColocationBundle\Entity\Colocation $colocation)
    {
        $this->colocations->removeElement($colocation);
    }

    /**
     * Get colocations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColocations()
    {
        return $this->colocations;
    }

    public function __construct()
    {
        parent::__construct();
        $this->demandes = new ArrayCollection();
        $this->colocations = new ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
}
