<?php


namespace AJM\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AJM\ColocationBundle\Form\DataTransformer\ArrayToStringTransformer;

class AdminUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new ArrayToStringTransformer();
        $builder->add($builder->create('roles',       ChoiceType::class, array(
            'choices'  => array(
                'admin.user' => 'ROLE_USER',
                'admin.admin' => 'ROLE_ADMIN',
            ), 'label' => 'admin.droits' ,'multiple' => false,))->addModelTransformer($transformer))
            ->add('ajouter',      SubmitType::class, array('label'=>'bouton.ajouter.ajouter'));;

    }

    public function getParent()
    {
        return 'AJM\UserBundle\Form\RegistrationType';
    }

}